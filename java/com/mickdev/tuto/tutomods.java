package com.mickdev.tuto;

import com.mickdev.tuto.proxy.CommonProxy;
import com.mickdev.tuto.util.Utils;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid=References.MOIDID,name=References.NAME,version=References.VERSION)
public class tutomods {
    @Mod.Instance(References.MOIDID)
    public static tutomods Instance;
@SidedProxy(serverSide =References.SERVER_PROXY_CLASS ,clientSide=References.Client_PROXY_CLASS)
   public static CommonProxy proxy;
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){
        Utils.getLogger().info("Pre Initialize");
        proxy.registerRenders();
    }
    @Mod.EventHandler
    public void init(FMLInitializationEvent event){
        Utils.getLogger().info("Initialize");

    }
    public void postinit(FMLPostInitializationEvent event){
        Utils.getLogger().info("Post Initialize");
    }
}
